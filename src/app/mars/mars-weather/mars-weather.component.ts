import { Component, OnInit } from '@angular/core';
import { MarsWeatherService } from 'src/app/services/mars-weather.service';
import { Weather } from 'src/app/models/weather';
import { AtmosphericTemperature } from 'src/app/models/atmosphericTemperature';

@Component({
	selector: 'app-mars-weather',
	templateUrl: './mars-weather.component.html',
	styleUrls: ['./mars-weather.component.css']
})
export class MarsWeatherComponent implements OnInit {
	meteos: Weather[];
	atmoSphereTemp: AtmosphericTemperature;
	constructor(private marsWeatherService: MarsWeatherService) {}

	ngOnInit() {
		this.getMarsWeather();
	}

	getMarsWeather() {
		this.marsWeatherService
			.getWeather()
			.subscribe((result: Weather[]) => (this.meteos = result));
	}
}
