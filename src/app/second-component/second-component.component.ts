import { Component, OnInit } from '@angular/core';
import { SecondApiService } from '../services/second-api.service';

@Component({
	selector: 'app-second-component',
	templateUrl: './second-component.component.html',
	styleUrls: ['./second-component.component.css']
})
export class SecondComponentComponent implements OnInit {
	constructor(private secondService: SecondApiService) {}

	ngOnInit() {
		this.secondService.getData();
	}
}
