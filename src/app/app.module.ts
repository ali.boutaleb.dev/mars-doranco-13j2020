import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MarsWeatherComponent } from './mars/mars-weather/mars-weather.component';
import { HeaderComponent } from './common/header/header.component';
import { FooterComponent } from './common/footer/footer.component';
import { SecondComponentComponent } from './second-component/second-component.component';

@NgModule({
	declarations: [
		AppComponent,
		MarsWeatherComponent,
		HeaderComponent,
		FooterComponent,
		SecondComponentComponent
	],
	imports: [BrowserModule, AppRoutingModule, HttpClientModule],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {}
