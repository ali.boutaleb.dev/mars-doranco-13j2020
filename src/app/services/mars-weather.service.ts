import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Weather } from '../models/weather';
import { AtmosphericTemperature } from '../models/atmosphericTemperature';
import { AtmosphericPressure } from '../models/atmospheric-pressure';
import { HorizontalWindSpeed } from '../models/horizontal-wind-speed';

@Injectable({
	providedIn: 'root'
})
export class MarsWeatherService {
	constructor(private http: HttpClient) {}

	// Get weather from api
	getWeather(): Observable<Weather[]> {
		let weathers: Weather[] = [];
		return this.http
			.get(
				'https://api.nasa.gov/insight_weather/?api_key=mmVk7P8SO5ouMjnW5oh3x1We02ICwi4SVh2VsnaL&feedtype=json&ver=1.0'
			)
			.pipe(
				map(data => {
					Object.entries(data).forEach(element => {
						console.log(data);
						if (element[1].AT && element[1].HWS) {
							weathers.push(
								new Weather(
									new Date(element[1].First_UTC),
									new Date(element[1].Last_UTC),
									new AtmosphericTemperature(
										element[1].AT.mn,
										element[1].AT.mx,
										element[1].AT.av,
										element[1].AT.ct
									),
									new AtmosphericPressure(
										element[1].PRE.mn,
										element[1].PRE.mx,
										element[1].PRE.av,
										element[1].PRE.ct
									),
									new HorizontalWindSpeed(
										element[1].HWS.mn,
										element[1].HWS.mx,
										element[1].HWS.av,
										element[1].HWS.ct
									)
								)
							);
						}
					});
					return weathers;
				})
			);
	}
}
