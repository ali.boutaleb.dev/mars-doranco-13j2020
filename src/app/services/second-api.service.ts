import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
	providedIn: 'root'
})
export class SecondApiService {
	constructor(private http: HttpClient) {}

	getData(): void {
		this.http
			.get(
				'https://api.nasa.gov/insight_weather/?api_key=mmVk7P8SO5ouMjnW5oh3x1We02ICwi4SVh2VsnaL&feedtype=json&ver=1.0'
			)
			.pipe(
				map(data => {
					console.log(data);
				})
			);
	}
}
