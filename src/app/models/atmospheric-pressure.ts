export class AtmosphericPressure {
	minimum: number;
	maximum: number;
	average: number;
	sample: number;

	constructor(
		minimum: number,
		maximum: number,
		average: number,
		sample: number
	) {
		this.minimum = minimum;
		this.maximum = maximum;
		this.average = average;
		this.sample = sample;
	}
}
