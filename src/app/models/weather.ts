import { AtmosphericTemperature } from './atmosphericTemperature';
import { AtmosphericPressure } from './atmospheric-pressure';
import { HorizontalWindSpeed } from './horizontal-wind-speed';

export class Weather {
	dateFirst: Date;
	dateLast: Date;
	atmosphericTemperature: AtmosphericTemperature;
	atmosphericPressure: AtmosphericPressure;
	horinzontalWindSpeed: HorizontalWindSpeed;

	constructor(
		dateFirst: Date,
		dateLast: Date,
		atmosphericTemperature: AtmosphericTemperature,
		atmosphericPressure: AtmosphericPressure,
		horinzontalWindSpeed: HorizontalWindSpeed
	) {
		this.dateFirst = dateFirst;
		this.dateLast = dateLast;
		this.atmosphericTemperature = atmosphericTemperature;
		this.atmosphericPressure = atmosphericPressure;
		this.horinzontalWindSpeed = horinzontalWindSpeed;
	}
}
