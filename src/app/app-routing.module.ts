import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MarsWeatherComponent } from './mars/mars-weather/mars-weather.component';

const routes: Routes = [{ path: '', component: MarsWeatherComponent }];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule]
})
export class AppRoutingModule {}
